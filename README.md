## JS-22 (Typescript-10) - Typescript example project
This is an example Typescript project covering multiple Typescript features. Based on UDEMY course: https://www.udemy.com/understanding-typescript.

These examples are focusing on **TypeScript with React**.

#### Steps for project with TypeScript and React:
* npm init
* npm install react react-dom --save
* (npm install -g typings)
* (typings install dt~react dt~react-dom --global --save)
* npm install --save-dev @types/react @types/react-dom
* npm install webpack ts-loader --save-dev
* npm install lite-server --save-dev
* npm install typescript --save-dev
* tsc --init

#### Used commands and libs:
* npm init
* npm install lite-server --save-dev
* tsc <filename.ts>
* tsc --init
* tsc
* tsc -w
* tsc --outFile app.js circleMath.ts rectangleMath.ts app.ts 
* tsc app.ts --outFile app.js // if we use reference 
* npm install systemjs --save
* npm install jquery --save 
* npm install -g typings
* typings install dt~jquery --global --save
* npm install --save-dev @types/jquery
* npm install --save-dev gulp gulp-typescript
* npm install --save-dev webpack ts-loader
* npm install --save-dev typescript
